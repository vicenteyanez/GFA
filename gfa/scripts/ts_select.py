#! /usr/bin/python3
"""
@author: Vicente Yáñez
@date: 2017

Extract the time series data giving a certain time range and area

"""
import os
import shutil


from gfa.gnss_analysis.TimeSeriesControl import TimeSeriesControl
from gfa.gnss_analysis.ModelControl import ModelControl
from gfa.load_param import Config
from gfa.log_config import Logger


def main(codigo, lon_min, lon_max, lat_min, lat_max, tmin, tmax):
    try:
        if lon_min > lon_max or lat_min > lat_max or tmin > tmax:
            raise ValueError()

    except(ValueError, IndexError) as err:
        print('A wild error had raised when GFA was reading the parameters!\
     Please, check your input parameter and restart the script')
        log = Logger()
        log.logger.error(err)
        exit()

    # path archivos
    ts_path = Config.config['PATH']['timeseries']
    output_dir = Config.config['PATH']['output_dir']
    lista_gps = '{}{}'.format(ts_path, Config.config['PATH']['ListaGPS'])
    series_dir = '{}{}'.format(ts_path, Config.config['PATH']['GPSdata'])
    save_dir = Config.config['PATH']['output_dir']
    generalsolution_dir = '{}{}'.format(output_dir,
                                        Config.config['PATH']['general_solution'])
    # optional: dir of the trajectory model
    model_dir = '{}Modelo/'.format(generalsolution_dir)
    m_ls = '{}resume.txt'.format(generalsolution_dir)

    # check if save_dir exist
    select_dir = '{}{}'.format(save_dir, codigo)
    if os.path.exists(select_dir):
        erase = input('{} exist. Do you want to erase it? y/n(n)'.format(
            select_dir))
        if erase == 'y':
            shutil.rmtree(select_dir)
            print('Directory removed')
        else:
            rename = input('Rename the identificator parameter:')
            codigo = rename
    try:
        ts = TimeSeriesControl(codigo, lista_gps, series_dir, save_dir)
        ts.load_estations(lon_min, lon_max, lat_min, lat_max, tmin, tmax)
        ts.savedata()

        # optional: if you have a trajectory model you can load the same way
        if os.path.exists(model_dir):
            model = ModelControl(codigo, m_ls, model_dir, save_dir)
            model.load_model(lon_min, lon_max, lat_min, lat_max, tmin, tmax)
            model.savedata()

    except() as err:
        print('A wild error had raised! Please, check your input param or go\
              and see the log')
        log = Logger()
        log.logger.error(err)
