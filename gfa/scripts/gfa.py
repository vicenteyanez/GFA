#! /usr/bin/python3

"""
@author: Vicente Yáñez
@date: 2017
"""

import click

import gfa.scripts.ts_select
import gfa.scripts.ts_build as ts_build
import gfa.scripts.ts_plot as ts_plot
import gfa.scripts.ts_vector as ts_vector
import gfa.scripts.ts_buildmodelall as ts_buildmodelall
import gfa.scripts.ui_buildmodel as ui_buildmodel
import gfa.scripts.gfa_configure as gfa_configure


@click.group()
def cli():
    pass


@cli.command()
def configure():
    """
    Function that creates a default parameters.ini file
    in the current directory
    """
    gfa_configure.main()
    return


@cli.command()
@click.option('--alias', default='default', help='identifier for your query')
@click.option('--lonmin', default=-180., help='Minimun longitude')
@click.option('--lonmax', default=180., help='Maximun longitude')
@click.option('--latmin', default=-90., help='Minimun latitude')
@click.option('--latmax', default=90., help='Minimun latitude')
@click.option('--tmin', default=1., help='Initial time')
@click.option('--tmax', default=3000., help='Final time')
def select(alias, lonmin, lonmax, latmin, latmax, tmin, tmax):
    """
    Extract the time series data giving a certain time range and area
    """
    gfa.scripts.ts_select.main(alias, lonmin, lonmax, latmin, latmax, tmin,
                               tmax)
    return


@cli.command()
@click.option('--alias', default='default', help='identifier for your query')
@click.option('--station', default="", help='Station name')
@click.option('--poly', default=1, help='Grade of the polynomial function')
@click.option('--fourier', default='',
              help='periods of the Fourier function')
@click.option('--jumps', default='', help='Step location(yyyy-mm-dd)')
@click.option('--logscale', default='1', help='Scale of the log function')
@click.option('--logstart', default='', help='Start of the log function\
(yyyy-mm-dd)')
def build(alias, station, poly, jumps, fourier, logscale, logstart):
    """
    Calculates the model for one station
    """
    ts_build.main(alias, station, poly, jumps, fourier, logscale, logstart)
    return


@cli.command()
@click.option('--alias', default='', help='identifier for your query')
@click.option('--station', default="", help='Station name')
@click.option('--model', default="", help='-m for model plot')
@click.option('--vector', default="", help='-v for vector plot')
def plot(alias, station, model, vector):
    """
    Plot the time series and optionally include the model and vectors
    """
    ts_plot.main(alias, station, model, vector)
    return


@cli.command()
@click.option('--alias', default='default', help='identifier for your query')
@click.option('--station', default="", help='Station name')
@click.option('--vtype', default="tangent", help='Vector type, tangent or fit')
@click.option('--period', default=[], help='Time or period of time, it has \
to be a comma separated list')
def vector(alias, station, vtype, period):
    """
    Script that calculates the displacement vector of one station
    in a certain time or period.
    """
    period = [float(s) for s in period.split(',')]
    ts_vector.main(alias, station, vtype, period)
    return


@cli.command()
@click.option()
def buildmodelall():
    """
    Script that calculates a automatic model for all the station
    """
    alias = 'general_solution'
    ts_buildmodelall.main(alias)
    return


@cli.command()
@click.option('--alias', default='default', help='identifier for your query')
@click.option('--station', default="", help='Station name')
def uimodel(alias, station):
    """
    Runs a manual script that calculates models with the user's parameters.
    """
    ui_buildmodel.main(alias, station)
    return
