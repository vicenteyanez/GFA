# GNSS Field Analysis (GFA)

My name is Vicente Yañez and I'm a geologist interested in geophysics and computer science. Right now I am doing a Master in Geophysics in the University of Chile.  The core of GFA is a compilation of python's functions that I made for my internship in [Centro Sismológico Nacional](http://www.sismologia.cl/) (CSN) and rutines I wrote during my undergraduate thesis.You can contact me through mail vicenteyanez@protonmail.com

GFA has the functions to calculate vectors from a gnss time series data with the trajectory model of [Bevis & Brown, 2014](https://link.springer.com/article/10.1007/s00190-013-0685-5) and with this, obtain a velocity field that represent the cortical deformation. Also, for the calculation of the velocity field, GFA has the distance-weighed function from [Cardozo & Allmendinger](http://www.sciencedirect.com/science/article/pii/S0098300408002410).After, from the velocity field this program include the tools to calculate the velocity tensor, and from this, the vorticity and stretching tensors (from the equations of [Davis & Titus, 2011](http://www.joshuadavis.us/teaching/2013fcomps/davistitus2011.pdf)).

__The GNSS stations time series data are not included in GFA.__

__If you want to make your own scripts to analyse the time series, you will find the core matematical funtions inside gfa/gnss_analysis/ModeloTrayectoria.py and field_analysis/field.py__

I am sorry if you read any misspells o grammatical error. English is my second lenguage and my skills are far from perfect.

## Principal commands and first steps
After install GFA (instructions below), you will have access to the console commands of GFA. It uses python click module (more details in click.pocoo.org). You can see all the commands them if you write:

```
gfa --help
```

### Set up your config and directories: parameters.ini file
```
gfa configure
```
This commands creates a parameter.ini file inside the local directory that you need to modify.
* output_dir: directory where you want to GFA save your results.
* timeseries: directoy where you have your time series data
  * gpsdata directory(*)
* listagps: name of the txt file inside your timeseries directory (only the name of the file) where you have the stations positions data.
  * First line is for comments (so is ignored).
  * Three columns separated by a space. StationsName Longitud Latitude.
*  eqfile:  = name of the file (fullpath) where you can put the most important sismic events in your zone. It is used in the automatic trajectory model solutions. 
  * Fist line is for comments
  * The columns are: lat_max lat_min fecha_evento    post_sismico
* gpsdata(*): directory inside the timeseries directory with the data of your stations.
  * Inside it should be a txt file for each stations, and the name of each file should be station_name.txt
  * No comments
  * There are 9 rows separated by 3 spaces. 
  * StationName   year   day_of_year   x_position   y_position   z_position   x_error   y_error   z_error
* general_solution: name of the directory (inside your output_dir) where the general solution was created.
* errmax: max error in the xyz position. If a position have more than this error it will be deleted 
* offline_username: if you are running GFA offline (locally), you can put any name you like.
* websession: False if your are running a offline session and True if you are running GFA on a server.

### GFA automatic solution for all the station in the db
```
gfa buildmodelall
```
The fist command you have to use. It creates a trajectory model for all the station in the db, named "general solution". Becouse this is an automatic solution, there are a lot of things that we can not modelate. The general idea is improve the model manually after one station at the time (see comands below). The results is saved in a directory named 'general_solution' inside your current directory.

### Select your stations
```
gfa select --alias --lonmin --lonmax --latmin --latmax --tmin --tmax
```

This commands uses the parameters given by the user to select the station inside an area in a specific time range. The results are saved inside a directory named --alias inside your current directory. All the rest of the gfa commands works inside this directory (output_dir/alias), so remember to give the same alias parameters to all the commands. It needs to have in your current directory a general_solution directory.

#### Example of use:
Select the GPS data between -75 and -65º longitude (west), -20 and -48º latitude (south) and between the years 2012 and 2015.
```
gfa select --alias example --lonmin -74 --lonmax -65 --latmin -48 --latmax -20 --tmin 2012.0 --tmax 2015.0
```

### Plot stations
```
gfa plot --alias --station --model --vector
```
This commands make a time series plot of your station. You can add the trajectory model putting -m in the --model parameter, also you can plot the vectors that you have calculated adding -v in the --vector parameter. A .png image will appear inside your output_directory/alias.

### Modify the trajectory model
```
gfa build --alias --station --poly --jumps --fourier --logscale --logstart
```
The build commands provides you a simple way to calculate the trajectory model using your own parameters. For more details about this parameters see the Bevis & Brown (2014). This command rewrites the trajectory model for your selected station. 

### Calculate a velocity vector
```
gfa vector --alias --station --vtype --period
```
With this command you can calculate a velocity vector for one station in a specific time (or period of time). The --vtype parameter could be tangent(if you give only one value in --period) or fit (if you give two values separated by comma in --period). The result is saved in the vector.txt file inside your alias directory.

### GNSS command line UI
```
gfa uimodel --alias --station
```

Finally, GFA also provides a command line UI to calculate the trajectory model. 

### Velocity field analysis

Through there is no command line command to calculate the vorticity or stretching field, you can use the python functions in gfa.field_analysis.field module. Or also you can use the GUI I am making (see next section)
## Andes 3DB
For the chilean investigation proyect "Active Tectonics and Volcanism at the Southern", a web based GUI was made. The GUI was developed using Python Flask. It provides an interface to
1. Select the GNSS station by position
2. Edit and calculate a trajectory model for each station
3. Calculate velocity vectors for differents time intervals.
4. Use this velocity vectors to calculate a velocity field and from this, visualizate the vorticity and streching field.

Screenshot of the selection section. Here you can select the GNSS station you want to use.
![alt text](https://github.com/VicenteYanez/GFA/blob/develop/static/images/homepage1.png?raw=true)

Screenshot of the edition section of the trajectory model parameters and time
![alt text](https://github.com/VicenteYanez/GFA/blob/develop/static/images/homepage2.png?raw=true)

Screenshot of the vorticity visualization on the map
![alt text](https://github.com/VicenteYanez/GFA/blob/develop/static/images/homepage3.png?raw=true)

## Collaborators
Andrés Tassara. Lead investigator of the chilean Fondecyt proyect 1151175 Active Tectonics and Volcanism at the Southern Andes. You can contact him to his mail andrestassara@udec.cl

Francisco García. Francisco is a geologist doing his doctoral thesis in Universidad de Concepción about the connection of sismicity and volcanic eruptions in the southern Andes. You can contact him to his mail franciscogarcia@udec.cl

You can contact me through mail vicenteyanez@protonmail.com

## Install

All the dependences necessary for run GFA are installable via pip. The use of virtualev is recommended.

```
sudo pip3 install virtualenv
mkdir my_project
cd my_project
virtualenv my_project_env
. my_project/bin/activate
sudo pip3 install gfa
```
Through pip install the required libraries automatically, sometimes I had some problems installing the cartopy library. If that is your case I recomend installing the libraries one by one using pip before install GFA.

### Requirements
* numpy
* scipy
* matplotlib
* cartopy
* click
* flask
* pandas

### Run GFA on a server

Althought this has not been properly tested, in theory the only thing inside gfa to run it on a server(appart from configure the server to work with flask) is edit the parameters.ini file. 
```
[WEB]
websession = True
offline_username = anything-you-like
```

### Use more than one GNSS database

This only works if you are using the web GUI. To config this you have to edit the parameters.ini in this way
```
[WEB]
databases = database1,database2,database_n
```
And have in your output directory, the general_solution of this databases named as: general_solution_database1, general_solution_database2, general_solution_database_n


## Roadmap
* Command to calculate vorticity from the command line.
* Add fourier spectre analysis to improve the calculation of the automatic solution.
* Add a pyinstaller file for distribution.
